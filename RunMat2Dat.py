import subprocess
import time

test_output_dir = '/home/humphries/data/FIRMM_test_results/Cross_realign_framewise/sub-02'
input_dir = '/home/humphries/data/ABCD_DTI/sub-NDARINVBL9KRALR/dwi_4dfp/unstacked/'
num_frames = 103
sub_id='02'

for frame_num in range(1, num_frames):
     fname = 'DWI_sub-{}_frame{}_xr3d.mat'.format(sub_id, str(frame_num))

     # skip frame 1
     if frame_num == 1:
         continue

     # find the previous frame and call cross_realign3d_4dfp -c to align this frame to the previous frame
     mat_dat_call = '/home/humphries/4dfp_tools/RELEASE/mat2dat {}'.format(fname)
     print(mat_dat_call)
     subprocess.call(mat_dat_call, shell = True)
     time.sleep(5)