#!/bin/bash

BASE_DIR="/home/humphries/data/FIRMM_test_results/Cross_realign_framewise"
TEMP_FILE="$BASE_DIR/sub-02_dwi_moco_temp.txt"
CR_MOCO="$BASE_DIR/sub-02_dwi_moco.txt"

rm -rf $CR_MOCO
touch $CR_MOCO
chmod 777 $CR_MOCO

rm -rf $TEMP_FILE
touch $TEMP_FILE
chmod 777 $TEMP_FILE

for frame in {2..102}; do
    file="$BASE_DIR/sub-02/DWI_sub-02_frame${frame}_xr3d.dat"
    echo $file
    grep -A 1 '#frame' $file >> $TEMP_FILE
done

head -n 1 $TEMP_FILE >> $CR_MOCO
grep [0-9] $TEMP_FILE >> $CR_MOCO

sed "s/^[ \t]*//" -i $CR_MOCO