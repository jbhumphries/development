#!/bin/bash

SUB_ID="NDARINVBL9KRALR"
SUB_DIR="/home/humphries/data/ABCD_DTI/sub-${SUB_ID}"
mkdir -p $sub/{ap_dcm,pa_dcm,dwi_dcm,all_dcm}
AP_DIR="${SUB_DIR}/baseline_year_1_arm_1/${SUB_ID}_*AP*_dicoms/*.dcm"
PA_DIR="${SUB_DIR}/baseline_year_1_arm_1/${SUB_ID}_*PA*_dicoms/*.dcm"
DWI_DIR="${SUB_DIR}/baseline_year_1_arm_1/${SUB_ID}_*DTI*_dicoms/*.dcm"

for file in $AP_DIR
do
f=$(basename $file)
cp $file "$SUB_DIR/ap_dcm/AP_$f"
cp $file "$SUB_DIR/all_dcm/AP_$f"
done

for file in $PA_DIR
do
f=$(basename $file)
cp $file "$SUB_DIR/pa_dcm/PA_$f"
cp $file "$SUB_DIR/all_dcm/PA_$f"
done

for file in $DWI_DIR
do
f=$(basename $file)
cp $file "$SUB_DIR/dwi_dcm/DWI_$f"
cp $file "$SUB_DIR/all_dcm/DWI_$f"
done
