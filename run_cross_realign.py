import os
import subprocess
import glob
import shutil

def run_cross_realign_framewise(test_output_dir, input_dir, num_frames, sub_id):
    """ Function to run cross_realign3d_4dfp on each set of consecutive frames

    test_output_dir : string
        path to output directory
    input_dir : string
        path to the directory containing the unaligned 4dfp images
    series :str
       series number; used to determine what the 4dfp images will be called
    num_frames : int
        number of frames you want to process

    """

    # Make the output directory if necessary
    if not os.path.isdir(test_output_dir):
        os.mkdir(test_output_dir)

    # for each frame of interest, find the 4dfp files associated with that frame in input_dir
    for frame_num in range(1, num_frames):
        #frame_name = 'b{}_f{}.4dfp'.format(series, str(frame_num))
        frame_name = 'DWI_sub-{}_frame{}.4dfp'.format(sub_id, str(frame_num))
        init_files = glob.glob(os.path.join(input_dir, frame_name + '.*'))

        # copy the 4dfp files into the output directory
        for f in init_files:
            _, f_name = os.path.split(f)
            f_new = os.path.join(test_output_dir, f_name)
            shutil.copyfile(f, f_new)

            # if this is the actual image file, store its path
            if f.endswith('.4dfp.img'):
                current_frame_path = f_new

        # skip frame 1
        if frame_num == 1:
            continue

        # find the previous frame and call cross_realign3d_4dfp -c to align this frame to the previous frame
        prev_frame = current_frame_path.replace("_frame"+str(frame_num), "_frame"+str(frame_num-1))
        cross_realign_call = '/home/humphries/4dfp_tools/RELEASE/cross_realign3d_4dfp -c -v0 {} {}'.format(prev_frame, current_frame_path)
        print(cross_realign_call)
        subprocess.call(cross_realign_call, shell = True)

if __name__ == "__main__":
    test_output_dir = '/home/humphries/data/FIRMM_test_results/Cross_realign_framewise/sub-02'
    input_dir = '/home/humphries/data/ABCD_DTI/sub-NDARINVBL9KRALR/dwi_4dfp/unstacked/'
    num_frames = 103
    series_num = 14
    sub_id='02'
    #b_value_list = [3000, 2000, 3000, 1000, 3000, 3000, 0, 3000, 2000, 3000, 3000, 1000, 3000, 500, 3000, 3000, 2000, 3000, 1000, 3000, 0]

    #run_cross_realign_bgroups(test_output_dir, input_dir, num_frames, b_value_list, series_num, first_frame = False)
    run_cross_realign_framewise(test_output_dir, input_dir, num_frames=num_frames, sub_id=sub_id)
