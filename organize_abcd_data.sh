#!/bin/bash

# Script to extract the zipped/nested raw abcd DTI data and then put all DTI dicoms in a single folder with names based on scan type

# To run:
# 1. Download some DTI ABCD data using abcd-dicom2bids (This script assumes you have only downloaded DTI data; may expand eventually)
# 2. Make sure you have the 7z executable installed ( sudo apt install p7zip-full)
# 3. Set the paths below for abcd_data_dir and firmm_data_dir
# 4. Run

# set the paths:
# abcd_data_dir --> the outer 'raw' directory where abcd data was downloaded
# firmm_data_dir -->where you want to move the abcd data once it has been unzipped
abcd_data_dir='/home/humphries/data/ABCD_DTI/'
firmm_data_dir='/home/humphries/data/FIRMM_test_data/DTI/ABCD_DTI/'

# Loop through the raw ABCD data and find participants whose data has not been copied over to the firmm_data_dir
abcd_part_dirs=$(ls $abcd_data_dir)
echo "Defined abcd_part_dirs:\n $abcd_part_dirs"
for abcd_part in ${abcd_part_dirs[@]}; do
  firmm_flat_dir="${firmm_data_dir}/${abcd_part}_flat"
  echo "FIRMM flat dir: $firmm_flat_dir"
  if [ -d ${abcd_data_dir}/${abcd_part} ] && [ ! -d $firmm_flat_dir  ] ; then

     # Skip the participant if they do not have a baseline directory
     base_line_dir="${abcd_data_dir}/${abcd_part}/baseline_year_1_arm_1"
     if [ ! -d $base_line_dir ]; then
       echo "WARNING: NO Directory ${base_line_dir}"
       continue
	 echo "Baseline dir: $base_line_dir"
     # if baseline data is present, make a firmm flat dir
     else
       echo "Getting data for ${abcd_part} ..."
       mkdir $firmm_flat_dir

       # For each folder in the baseline directory, extract its data to an 'extracted' folder
       base_line_dir_files=$(ls $base_line_dir)
       for base_line_file in $base_line_dir_files; do
          extracted_folder_name=${base_line_file/'.tgz'/'_extracted'}
          extracted_folder_path="$base_line_dir/${extracted_folder_name}"
          7z e "$base_line_dir/${base_line_file}" -r -o$extracted_folder_path

          # Run 7z again on the extracted folder to get the dicoms into a 'dicom' folder
          dicom_dir=${extracted_folder_path/'_extracted'/'_dicoms'}
          7z e $extracted_folder_path -r -o$dicom_dir

          # set the prefix for the new dicom names based on the type of scan
          if [[ $dicom_dir == *"DTI"* ]]; then
            prefix='dwi'
          elif [[ $dicom_dir == *"Diffusion-FM-PA"* ]]; then
            prefix='PA_map'
          elif [[ $dicom_dir == *"Diffusion-FM-AP"* ]]; then
            prefix='AP_map'
          else
            echo "ERROR: Unrecognized dicom folder ${dicom_dir}"
            exit
          fi

          # move the dicoms from the dicom folder into the FIRMM_flat folder, renaming them with prefix
          dicom_files=$(ls $dicom_dir)
          for dcm in $dicom_files; do
            if [[ $dcm == *"dicom"* ]]; then
              cp "${dicom_dir}/${dcm}" "${firmm_flat_dir}/$prefix$dcm"
            fi
          done
       done
     fi
  fi
done
