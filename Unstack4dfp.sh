#!/bin/bash
SUB_ID="NDARINVBL9KRALR"
IN="/home/humphries/data/ABCD_DTI/sub-$SUB_ID/dwi_4dfp/DWI_sub-02"
OUT_DIR="/home/humphries/data/ABCD_DTI/sub-$SUB_ID/dwi_4dfp/unstacked"

mkdir -p $OUT_DIR

#check number of frames in dicom with python: nibabel.load, data.shape
frame=1
while [ $frame -le 103 ]
do
    /home/humphries/4dfp_tools/RELEASE/extract_frame_4dfp $IN $frame
    frame=$(( $frame + 1 ))
done