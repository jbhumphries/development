#!/bin/bash
SUB_ID="NDARINVBL9KRALR"
IN_DIR="/home/humphries/data/ABCD_DTI/sub-$SUB_ID/dwi_dcm"
OUT_DIR="/home/humphries/data/ABCD_DTI/sub-$SUB_ID/dwi_4dfp"
FRAME=1

for file in $IN_DIR/*.dcm; do
    /home/humphries/4dfp_tools/RELEASE/dcm_to_4dfp -g -s T -b $OUT_DIR/DWI_sub-02_frame$FRAME $file
    let "FRAME++"
done
